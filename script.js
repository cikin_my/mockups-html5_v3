$(document).ready(function () {
     
     
      $('#step1').delay(6500).fadeIn(2000);

      $('.btn-section').removeClass('active');
      var myPlayer = videojs('my-video');     

      myPlayer.on("ended", function () {
            // alert('The video has ended');
            $('.btn-section').addClass('active');
      });

      $("#login").click(function (event) {
            event.preventDefault();

            $('.main-container').hide();
            $('#step1').hide();
            $('#step2').delay(500).fadeIn(2000);
      });
      $("#btn-step2").click(function (event) {
            event.preventDefault();

            $('.main-container').hide();
            $('#step1').hide();
            $('#step2').hide();
            myPlayer.dispose();
            $('#step3').delay(500).fadeIn(2000);
      });
      $("#btn-step3").click(function (event) {
            event.preventDefault();

            $('.main-container').hide();
            $('#step1').hide();
            $('#step2').hide();
            $('#step3').hide();
            $('#step4').delay(500).fadeIn(2000);           

      });
    
      


});
